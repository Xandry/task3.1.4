package com.example.resttemplate314;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class RestTemplate314Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(RestTemplate314Application.class, args);
        Runner runner = applicationContext.getBean("runner", Runner.class);
        System.out.println(runner.getTaskKey());
    }

}
