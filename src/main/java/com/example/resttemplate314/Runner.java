package com.example.resttemplate314;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class Runner {

    private final User james = new User(3L, "James", "Brown", (byte) 32);
    private final User thomas = new User(3L, "Thomas", "Shelby", (byte) 32);

    @Value("${url}")
    private String urlApi;

    public String getTaskKey() {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = restTemplate.headForHeaders(urlApi);
        String cookie = httpHeaders.get("Set-Cookie").get(0);
        HttpHeaders httpHeadersToSend = new HttpHeaders();
        httpHeadersToSend.set("Cookie", cookie + ";");

        HttpEntity<User> request1 = new HttpEntity<>(james, httpHeadersToSend);
        ResponseEntity<String> response1 = restTemplate.exchange(urlApi, HttpMethod.POST, request1, String.class);
        String part1 = response1.getBody();

        HttpEntity<User> request2 = new HttpEntity<>(thomas, httpHeadersToSend);
        ResponseEntity<String> response2 = restTemplate.exchange(urlApi, HttpMethod.PUT, request2, String.class);
        String part2 = response2.getBody();

        HttpEntity<Void> request3 = new HttpEntity<>(httpHeadersToSend);
        ResponseEntity<String> response3 = restTemplate.exchange(urlApi + "/3", HttpMethod.DELETE, request3, String.class);
        String part3 = response3.getBody();

        return part1 + part2 + part3;
    }

}
